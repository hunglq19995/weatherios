//
//  Presenter.swift
//  WeatheriOS
//
//  Created by Le Quang Hung on 01/03/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import Moya

protocol LoadDataDelegate {
    func beforeTappedSendButton()
    func afterTappedSendButton()
    func sendSuccess()
    func unsuccess(message: String)
}

class Presenter {
    var delegate: LoadDataDelegate
    var service = Service()
    init(delegate: LoadDataDelegate) {
        self.delegate = delegate
    }
    
    let moyaProvider = MoyaProvider<WeatherService>()
    var cityWeather: CityWeather?
    
    func getDataFromServer(cityName: String) {
        if cityName.isEmpty {
            self.delegate.unsuccess(message: "Ten thanh pho dang trong")
            return
        }
        else {
            moyaProvider.request(.getWeatherDataByCityName(cityName: cityName)) { (response) in
                switch response {
                case .success(let value):
                    do {
                        let jsonObject = try value.mapJSON()
                        self.cityWeather = Mapper<CityWeather>().map(JSONObject: jsonObject)
                        self.delegate.afterTappedSendButton()
                        self.delegate.sendSuccess()
                        
                        if self.cityWeather?.cityName == nil
                        {
                            self.delegate.unsuccess(message: "K co thanh pho vua nhap")
                        }
                    }
                    catch {
                        print(error)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
}
