//
//  ViewController.swift
//  WeatheriOS
//
//  Created by Le Quang Hung on 28/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import ObjectMapper

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var sunriseTime: UILabel!
    @IBOutlet weak var sunsetTime: UILabel!
    @IBOutlet weak var chanceOfRain: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var feelLikesTemp: UILabel!
    @IBOutlet weak var precipitation: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var visibility: UILabel!
    @IBOutlet weak var uvIndex: UILabel!
    
    
    var presenter: Presenter?
    var cityWeather: CityWeather?
    let moyaProvider = MoyaProvider<WeatherService>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.presenter = Presenter(delegate: self)
        beforeTappedSendButton()
    }
    
    @IBAction func buttonSend(_ sender: Any) {
        presenter?.getDataFromServer(cityName: textFieldCity.text!)
        
    }
}


extension ViewController: LoadDataDelegate {
    func beforeTappedSendButton() {
        labelTemp.isHidden = true
        weatherDescription.isHidden = true
        cityName.isHidden = true
        sunriseTime.isHidden = true
        sunsetTime.isHidden = true
        chanceOfRain.isHidden = true
        humidity.isHidden = true
        wind.isHidden = true
        feelLikesTemp.isHidden = true
        precipitation.isHidden = true
        pressure.isHidden = true
        visibility.isHidden = true
        uvIndex.isHidden = true
    }
    
    func afterTappedSendButton() {
        labelTemp.isHidden = false
        weatherDescription.isHidden = false
        cityName.isHidden = false
        sunriseTime.isHidden = false
        sunsetTime.isHidden = false
        chanceOfRain.isHidden = false
        humidity.isHidden = false
        wind.isHidden = false
        feelLikesTemp.isHidden = false
        precipitation.isHidden = false
        pressure.isHidden = false
        visibility.isHidden = false
        uvIndex.isHidden = false
    }
    
    func sendSuccess() {
        let temp = (presenter?.cityWeather?.temp ?? 0) - 273
        labelTemp.text = "\(temp)º"
        weatherDescription.text = presenter?.cityWeather?.description
        cityName.text = presenter?.cityWeather?.cityName
        let pressureInt = presenter?.cityWeather?.pressure ?? 0
        pressure.text = "\(pressureInt) hPa"
        let humidityInt = presenter?.cityWeather?.humidity ?? 0
        humidity.text = "\(humidityInt)%"
        let visibilityInt = presenter?.cityWeather?.visibility ?? 0
        visibility.text = "\(visibilityInt/1000) km"
        let windDeg = presenter?.cityWeather?.windDeg ?? 0
        let windSpeed = presenter?.cityWeather?.windSpeed ?? 0
        wind.text = "\(windDeg)  \(windSpeed) km/h"
    
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+7") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        
        //sunrise
        let sunriseDate = Date(timeIntervalSince1970: presenter?.cityWeather?.sunrise ?? 0)
        let sunrise = dateFormatter.string(from: sunriseDate)
        sunriseTime.text = sunrise
        
        //sunset
        let sunsetDate = Date(timeIntervalSince1970: presenter?.cityWeather?.sunset ?? 0)
        let sunset = dateFormatter.string(from: sunsetDate)
        sunsetTime.text = sunset
        
    }
    
    func unsuccess(message: String) {
        cityName.isHidden = false
        cityName.text = message
        labelTemp.isHidden = true
        weatherDescription.isHidden = true
        sunriseTime.isHidden = true
        sunsetTime.isHidden = true
        chanceOfRain.isHidden = true
        humidity.isHidden = true
        wind.isHidden = true
        feelLikesTemp.isHidden = true
        precipitation.isHidden = true
        pressure.isHidden = true
        visibility.isHidden = true
        uvIndex.isHidden = true
        
    }
    
}






