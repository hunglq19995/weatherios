//
//  Example.swift
//  Weather
//
//  Created by Le Quang Hung on 12/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation
import ObjectMapper

struct CityWeather: Mappable {
    var cityName: String?
    var temp: Int?
    var country: String?
    var description: String?
    var sunrise: Double?
    var sunset: Double?
    var humidity: Int?
    var pressure: Int?
    var visibility: Int?
    var windSpeed: Int?
    var windDeg: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        cityName <- map["name"]
        temp <- map["main.temp"]
        country <- map["sys.country"]
        description <- map["weather.0.description"]
        sunrise <- map["sys.sunrise"]
        sunset <- map["sys.sunset"]
        humidity <- map["main.humidity"]
        pressure <- map["main.pressure"]
        visibility <- map["visibility"]
        windDeg <- map["wind.deg"]
        windSpeed <- map["wind.speed"]
    }
}
