//
//  MoyaDef.swift
//  WeatheriOS
//
//  Created by Le Quang Hung on 04/03/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation
import Moya

enum WeatherService {
    case getWeatherDataByCityName(cityName: String)
    case getWeatherDataByCityID(id: String)
    case getWeatherDataByCoordinates(lat: Double, lon: Double)
}

extension WeatherService: TargetType {
    var baseURL: URL {
        return URL(string: Constants().baseURL)!
    }
    
    var path: String {
        switch self {
        case .getWeatherDataByCityName:
            return "data/2.5/weather"
        case .getWeatherDataByCityID:
            return "data/2.5/weather"
        case .getWeatherDataByCoordinates:
            return "data/2.5/weather"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getWeatherDataByCityName(let cityName):
            return .requestParameters(parameters: ["q": cityName, "appid": Constants().appid], encoding: URLEncoding.default)
        case .getWeatherDataByCityID(let id):
            return .requestParameters(parameters: ["id": id, "appid": Constants().appid], encoding: URLEncoding.default)
        case .getWeatherDataByCoordinates(let lat, let lon):
            return.requestParameters(parameters: ["lat": lat, "lon": lon, "appid": Constants().appid], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
