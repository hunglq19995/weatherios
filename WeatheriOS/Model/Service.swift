//
//  Service.swift
//  WeatheriOS
//
//  Created by Le Quang Hung on 01/03/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation
import Alamofire
import Moya
import ObjectMapper

class Service {
    
    let moyaProvider = MoyaProvider<WeatherService>()
    var cityWeather: CityWeather?
    
    func getDataFromServer(cityName: String) {
        moyaProvider.request(.getWeatherDataByCityName(cityName: cityName)) { (response) in
            switch response {
            case .success(let value):
                print(value)
            case .failure(let error):
                print(error)
            }
        }
    }
}

